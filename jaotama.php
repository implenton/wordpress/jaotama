<?php
/*
 * Plugin Name:       Jaotama
 * Description:       Social sharing links for custom themes
 * Plugin URI:        https://implenton.com/
 * Version:           1.2.0
 * Author:            implenton
 * Author URI:        https://implenton.com/
 * License:           GPLv3
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
 * GitLab Plugin URI: implenton/wordpress/jaotama
 */

namespace Jaotama;

function get_registered_social_services() {

    return apply_filters( 'jaotama_social_services', [
        'facebook',
        'twitter',
        'email',
    ] );

}

require plugin_dir_path( __FILE__ ) . 'service-url-builders.php';

function display( $class = null, $show_title = null, $icon = null ) {

    $social_services = get_registered_social_services();

    // If there are no social services to display abort!
    if ( ! $social_services ) {
        return;
    }

    // So we don't break anything
    $extra_class = '';

    // If no class is provided we get the default class or the filtered one
    if ( is_null( $class ) ) {
        $main_class = apply_filters( 'jaotama_class', 'jaotama' );
    } else {
        // We accept either a string or an array of classes
        if ( gettype( $class ) === 'array' ) {
            // First class is the main class, all child elements inherit that
            $main_class = $class[0];
            // Maybe they passed an array with a single value
            if ( count( $class ) > 1 ) {
                // Implode because we don't know how many classes they passed
                unset( $class[0] );
                $extra_class = implode( ' ', $class );
            }
        }

        if ( gettype( $class ) === 'string' ) {
            $main_class = $class;
        }
    }

    // If no icon provided, we get the default value for icon which can be filtered
    if ( is_null( $icon ) ) {
        $icon = apply_filters( 'jaotama_show_icon', true );
    }

    if ( is_null( $show_title ) ) {
        $show_title = apply_filters( 'jaotama_show_title', true );
    }

    $item = '';

    foreach ( $social_services as $social_service ) {
        // Lets sanitize the user input they might used the jaotama_social_services filter
        $service_name = sanitize_title( $social_service );

        $item .= build_element( $social_service, $main_class, $icon );
    }

    $title_class = apply_filters( 'jaotama_title_class', $main_class . '__title', $main_class );
    $list_class  = apply_filters( 'jaotama_list_class', $main_class . '__list', $main_class );

    require plugin_dir_path( __FILE__ ) . 'display.php';
}

function determine_page_url() {
    // If it was called in the loop they probably want to share the post; we can safely use the permalink
    if ( in_the_loop() ) {
        return apply_filters( 'jaotama_page_url_loop', get_permalink() );
    }

    return apply_filters( 'jaotama_page_url', home_url( add_query_arg( null, null ) ) );
}

function determine_page_title() {
    return apply_filters( 'jaotama_page_title', wp_get_document_title() );
}

function build_element( $service, $class, $icon ) {
    $service_function = str_replace( '-', '_', $service );

    /*
     * Check for the function first because they might used the jaotama_social_services filter
     * and might have passed services without writing a url builder function
     */
    if ( ! function_exists( __NAMESPACE__ . "\get_{$service_function}_url" ) ) {
        return;
    }

    $url                   = call_user_func( __NAMESPACE__ . "\get_{$service_function}_url" );
    $target                = apply_filters( 'jaotama_link_target', '_self' );
    $class_el              = apply_filters( 'jaotama_el_class', $class . '__el', $class );
    $class_label           = apply_filters( 'jaotama_label_class', $class . '__label', $class );
    $class_ico             = apply_filters( 'jaotama_icon_class', $class . '__ico', $class );
    $window_opener         = apply_filters( 'jaotama_window_opener', true );
    $window_opener_class   = apply_filters( 'jaotama_window_opener_class', 'jaotama__window-opener' );
    $window_opener_exclude = apply_filters( 'jaotama_window_opener_exclude', [ 'email', ] );

    ob_start();

    ?>

    <li class="<?php echo esc_attr( $class_el ) . ' ' . esc_attr( $class_el ) . '--' . esc_attr( $service ); ?>">

        <a<?php echo ( $window_opener && ! in_array( $service, $window_opener_exclude ) ) ? ' class="' . $window_opener_class . '"' : ''; ?>
                href="<?php echo esc_attr( $url ); ?>"
                target="<?php echo esc_attr( $target ); ?>">

            <span class="<?php echo esc_attr( $class_label ); ?>">
                <?php echo apply_filters( 'jaotama_share_text', __( 'Share it with ', 'jaotama' ) . $service, $service ); ?>
            </span>

            <?php if ( $icon ) :

                $icon_found = load_icon( $service );

                if ( $icon_found ): ?>

                    <span class="<?php echo esc_attr( $class_ico ); ?>">
                        <?php echo load_icon( $service ); ?>
                    </span>

                <?php endif;

            endif; ?>

        </a>

    </li>

    <?php

    return ob_get_clean();
}

function load_icon( $icon ) {
    $prefix    = apply_filters( 'jaotama_icon_prefix', '' );
    $location  = apply_filters( 'jaotama_icon_location', 'assets/svg/' );
    $file_type = apply_filters( 'jaotama_icon_file_type', '.svg' );
    $file      = get_theme_file_path( $location . $prefix . $icon . $file_type );

    return file_exists( $file ) ? file_get_contents( $file ) : false;
}

function window_opener() {
    $window_opener = apply_filters( 'jaotama_window_opener', true );

    if ( ! $window_opener ) {
        return;
    }

    wp_register_script(
        'jaotama-window-opener', plugin_dir_url( __FILE__ ) . 'jaotama-window-opener.js',
        null,
        '1.2.0',
        true
    );

    $window_opener_class = apply_filters( 'jaotama_window_opener_class', 'jaotama__window-opener' );

    wp_enqueue_script( 'jaotama-window-opener' );
    wp_add_inline_script( 'jaotama-window-opener', 'var jaotama_window_opener_class = ".' . $window_opener_class . '";', 'before' );
}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\window_opener' );
